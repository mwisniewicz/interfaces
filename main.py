print("### GENERATORY, DEKORATORY, METODY MAGICZNE NA KLASACH ###")
'''
def my_generator(n):
    for i in range(n):
        yield i*2

j = 0
for i in my_generator(10):
    print(i)

with open("plik.txt") as file:
    for i in range(10):
        print(file.readline())


example = [1, 2, 3, 4]
numbers = (number ** 2 for number in example)
for i in range(10):
    print(i)
    try:
        print(next(numbers))
    except StopIteration:
        print("KONIEC")
        break
'''

class Vector:
    """ KLASA """

    def __init__(self, n):
        print("__INIT__")
        self.n = n
        self.elements = []
        self.example = [1, 2, 3, 4, 5]

    def __str__(self):
        print("__STR__")
        return f'Vector class [{self.n}]'

    def __repr__(self):
        print("__REPR__")
        return f'Vector class [{self.n}]'

    def __getitem__(self, index):
        print("__GETITEM__")
        return self.example[index]

    def __setitem__(self, index, value):
        print("__SETITEM__")
        self.example[index] = value

    def __len__(self):
        return len(self.example)

    def items(self):
        for date, value in self.weather.items():
            yield (date, value)

x = Vector(n=3)
print(len(x))
print(x[0])
x[0] = 3
print(x[0])

class Book:

    def __init__(self, title, pages):
        self.title = title
        self.pages = pages
        self.example = {
            '2021-08-21': 'Bedzie padac',
            '2021-08-22': 'Nie bedzie padac'
            }

    def __eq__(self, object2):
        return self.pages == object2.pages

    def __gt__(self, object2):
        return self.pages > object2.pages

    def __iter__(self):
        for date in self.example.keys():
            yield date

book1 = Book("Pan T", 232)

print(list(book1))

'''
book2 = Book("Pan M", 234)

print(book1 < book2)
'''